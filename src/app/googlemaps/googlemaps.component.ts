import { Component, ElementRef } from '@angular/core';
import { GoogleMap } from '@capacitor/google-maps';

@Component({
  selector: 'app-googlemaps',
})
export class GoogleMapsComponent{

  constructor() { }

  mapRef: ElementRef<HTMLElement>;
  newMap: GoogleMap;

  async createMap(mapElement: any, latitude: number, longitude: number) {
    this.newMap = await GoogleMap.create({
      id: 'my-cool-map',
      element: mapElement,
      apiKey: 'AIzaSyB9tQhnAbQWN74Vd2jOnV3Z-lOnV8f5Aho',
      config: {
        center: {
          lat: latitude,
          lng: longitude,
        },
        zoom: 10,
      },
    });
    this.newMap.addMarker({
      coordinate: {
        lat: latitude,
        lng: longitude
      }
    });
  }
  
}
