import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'antennes', pathMatch: 'full' },
  { path: 'antennes', loadChildren: () => import('./antenne/antenne.module').then( m => m.AntennePageModule)},
  { path: 'antennes/:id', loadChildren: () => import('./antenne-detail/antenne-detail.module').then( m => m.AntenneDetailPageModule)},
  { path: 'near', loadChildren: () => import('./geoloc/geoloc.module').then( m => m.GeolocPageModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}
