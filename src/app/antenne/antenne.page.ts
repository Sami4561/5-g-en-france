import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AntenneService, SearchType } from '../services/antenne.service';
import { Observable } from 'rxjs';
import { Antenne } from './antenne';
import { GeolocService } from '../services/geoloc.service';
import { AngularDelegate } from '@ionic/angular';

@Component({
  selector: 'app-antenne',
  templateUrl: './antenne.page.html',
  styleUrls: ['./antenne.page.scss'],
})
export class AntennePage implements OnInit {
 
  public list: Antenne[] = [];
  depNumber: number;
  type: SearchType = SearchType.all;
  operateur: string;
  frequence: string;
  region: string;
  distance: number = -1;
  @Input() near: boolean = false; 
  @Input() userLat: number;
  @Input() userLong: number;
 
  constructor(private antenneService: AntenneService, private geolocService: GeolocService) {  }

  ngOnInit() { 
      this.antenneService.getAllAntennes(this.near)
    .subscribe(antennes => {
      if (!this.near) {
        this.list = antennes as Antenne[];
      }
    })
  }

  loadMore(infiniteScroll) {
    this.antenneService.getMoreAntennes(this.list.length)
    .subscribe(antennes => {
        this.computeDistances(antennes);
    })
   infiniteScroll.target.complete();
  }
 
  searchChanged() {
    this.antenneService.searchData(this.depNumber
      )
    .subscribe(antennes => {
        this.list = antennes as Antenne[];
    })
  }

  filterByOperateur() {
      this.antenneService.getAllAntennes(this.near, this.operateur, this.frequence, this.region)
      .subscribe(antennes => {
        this.computeDistances(antennes);
    })
  }
  
  changeRegion() {
    this.antenneService.getAllAntennes(this.near, this.operateur, this.frequence, this.region)
      .subscribe(antennes => {
        this.computeDistances(antennes);   
    })
  }

  changeDistance() {
    this.antenneService.getAllAntennes(this.near, this.operateur, this.frequence, this.region)
      .subscribe(antennes => {
          this.computeDistances(antennes);           
    })
  }

  computeDistances(antennes: Antenne[]){
    this.list = [];
    if (this.distance == -1) {
      for (let antenne of antennes) {  
        let distance = this.geolocService.getDistanceFromLatLonInKm(antenne.fields.geo_point_2d[0], antenne.fields.geo_point_2d[1], this.userLat, this.userLong);
        antenne.distance = Math.round(distance);
        this.list.push(antenne);
      }
    }
    else {
      for (let antenne of antennes) {  
        let distance = this.geolocService.getDistanceFromLatLonInKm(antenne.fields.geo_point_2d[0], antenne.fields.geo_point_2d[1], this.userLat, this.userLong);
        if (distance < this.distance) {   
          antenne.distance = Math.round(distance);
          this.list.push(antenne);
        }    
      }  
    }
    this.list.sort((a, b) => a.distance < b.distance ? -1 : a.distance > b.distance ? 1 : 0)  
  }

  filterByFrequence() {
    this.antenneService.getAllAntennes(this.near, this.operateur, this.frequence, this.region)
      .subscribe(antennes => {
          this.computeDistances(antennes);           
    })
  }

  changeSearchMode() {
    this.antenneService.changeSearchMode(this.operateur);
  }
}
