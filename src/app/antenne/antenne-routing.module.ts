import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AntennePage } from './antenne.page';

const routes: Routes = [
  {
    path: '',
    component: AntennePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AntennePageRoutingModule {}
