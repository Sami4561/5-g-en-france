import { DatePipe } from "@angular/common";

export interface BaseAntenne {
    datasetid: string;
    recordid: string;
    fields: {
        op_site_id: string;
        op_name: string;
        release_date_5g: DatePipe;
        dep_name: string;
        reg_name: string;
        reg_code: number;
        geo_point_2d: [
          number,
          number,
        ];
        com_name: string;
        op_code: number;
        geo_shape: {
            coordinates: [
                number,
                number,
              ];
            type: string;
        };
        com_code: number;
        frequency: string;
        dep_code: number;
        anfr_station_id: number;
        epci_code: number;
        epci_name: string;
      };
    geometry: {
        type: string;
        coordinates: [
            number,
            number,
          ];
    record_timestamp: string;
    }
}