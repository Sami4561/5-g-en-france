import { DatePipe } from "@angular/common";
import * as internal from "stream";
import { BaseAntenne } from "./base-antenne";

export class Antenne implements BaseAntenne {
    datasetid: string;
    recordid: string; //Id de l'antenne
    fields: {
        op_site_id: string;
        op_name: string; // Opérateur
        release_date_5g: DatePipe; // Date d'installation de l'antenne
        dep_name: string; // Nom de département
        reg_name: string; // Nom de région
        reg_code: number; // Code région
        geo_point_2d: [ // Coordonnées GPS (couple latitude + longitude)
          number,
          number,
        ];
        com_name: string; // Commune
        op_code: number;
        geo_shape: {
            coordinates: [
                number,
                number,
              ];
            type: string;
        };
        com_code: number;
        frequency: string; // Fréquence(s) supportée(s) par l'antenne
        dep_code: number; // Numéro de département
        anfr_station_id: number;
        epci_code: number;
        epci_name: string; // Intercommunauté
      };
    geometry: {
        type: string;
        coordinates: [
            number,
            number,
          ];
    record_timestamp: string;
    }
  distance: number;

    constructor(baseAntenne: BaseAntenne) {
      // On a uniquement besoin des champs fields ainsi que de l'id de l'antenne
        this.recordid = baseAntenne.recordid;
        this.fields = baseAntenne.fields;
    }
}