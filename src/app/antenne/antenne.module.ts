import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AntennePageRoutingModule } from './antenne-routing.module';

import { AntennePage } from './antenne.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AntennePageRoutingModule
  ],
  declarations: [AntennePage],
})
export class AntennePageModule {}
