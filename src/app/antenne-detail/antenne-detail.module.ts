import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AntenneDetailPageRoutingModule } from '../antenne-detail/antenne-detail-routing.module';
import { AntenneDetailPage } from './antenne-detail.page';
import { GoogleMapsComponent } from '../googlemaps/googlemaps.component'
import { registerLocaleData } from '@angular/common';;
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AntenneDetailPageRoutingModule
  ],
  declarations: [AntenneDetailPage, GoogleMapsComponent],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'},
  ]
})
export class AntenneDetailPageModule {}
