import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AntenneDetailPage } from './antenne-detail.page';

const routes: Routes = [
  {
    path: '',
    component: AntenneDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AntenneDetailPageRoutingModule {}
