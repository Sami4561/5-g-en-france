import { AntenneService } from '../services/antenne.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GoogleMapsComponent } from '../googlemaps/googlemaps.component';
import { Antenne } from '../antenne/antenne';

 
@Component({
  selector: 'app-antenne-detail',
  templateUrl: './antenne-detail.page.html',
  styleUrls: ['./antenne-detail.page.scss'],
})
export class AntenneDetailPage implements OnInit {
    
  antenne: Antenne = null;
  @ViewChild(GoogleMapsComponent) map;
  
  /**
   * Constructor of our details page
   * @param activatedRoute Information about the route we are on
   * @param antenneService The antenne Service to get data
   */
  constructor(private activatedRoute: ActivatedRoute, private antenneService: AntenneService) { }
 
  ngOnInit() {
    // Get the ID that was passed with the URL
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    // Get the antenne from the API
    // this.antenne = this.antenneService.getDetails(id);
    this.antenneService.getDetails(id).subscribe(result => {
      this.antenne = result[0];
      this.createMap(); 
    });
    
    //this.mapComponent.addMarker(this.antenne.records[0].fields.geo_shape.coordinates[0][1], this.antenne.records[0].fields.geo_shape.coordinates[0][0]);
  }

  createMap() {
    let mapElement = document.getElementById('map');
    this.map.createMap(mapElement, this.antenne.fields.geo_point_2d[0], this.antenne.fields.geo_point_2d[1]);
  }

}
