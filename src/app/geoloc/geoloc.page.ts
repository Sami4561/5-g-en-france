import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GeolocService } from '../services/geoloc.service';
import { Geolocation } from '@capacitor/geolocation';

@Component({
  selector: 'app-geoloc',
  templateUrl: './geoloc.page.html',
})
export class GeolocPage implements OnInit {
  public data: number;
  near: boolean = true;
  userLat: number;
  userLong: number;
  

  constructor(private geolocService: GeolocService) { 
    Geolocation.getCurrentPosition().then((resp) => {
      this.userLat = resp.coords.latitude;
      this.userLong = resp.coords.longitude;
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     

  }
 
  ngOnInit() { 
  }

}
