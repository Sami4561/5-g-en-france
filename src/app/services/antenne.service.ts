import { ComponentFactoryResolver, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { Antenne } from '../antenne/antenne';
 
// Typescript custom enum for search types (optional)
export enum SearchType {
  all = '',
  commune = 'com_name',
  frequence = 'frequency',
  operateur = 'op_name'
}
 
@Injectable({
  providedIn: 'root'
})
export class AntenneService { 
  searchType = '';
  element;
  antenneDetail: Antenne;
  operateur: string = '';
  frequence: string = '';
  region: string = '';
  distance: number;
  url = `https://public.opendatasoft.com/api/records/1.0/search/?dataset=buildingref-france-arcep-mobile-site-5g&q=&refine.op_nam${encodeURI(this.operateur)}&refine.frequenc${encodeURI(this.frequence)}&refine.reg_nam${encodeURI(this.region)}&rows=100`;
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient) { }
 
  refreshUrl() {
    this.url = `https://public.opendatasoft.com/api/records/1.0/search/?dataset=buildingref-france-arcep-mobile-site-5g&q=&refine.op_nam${encodeURI(this.operateur)}&refine.frequenc${encodeURI(this.frequence)}&refine.reg_nam${encodeURI(this.region)}&rows=100`;
  }

  getAllAntennes(near: boolean, operateur?: string, frequence?: string, region?: string): Observable<Antenne[]> {
    if (typeof operateur !== 'undefined'){
      if (operateur != 'Tous') {
        this.operateur = operateur;
        this.refreshUrl();
      }
      else {
        this.operateur = '';
      }     
    }
    if (typeof frequence !== 'undefined'){
      if (frequence != 'Toutes') {
        this.frequence = frequence;
        this.refreshUrl();
      }
      else {
        this.frequence = '';
      }    
    }
    if (near) {
      this.region = region;
      this.refreshUrl();
      return this.http.get<Antenne[]>(`${this.url}`)
      .pipe(map((results: Antenne[]) => results['records'].map(antenne => new Antenne(antenne)))
    );
    }
    return this.http.get<Antenne[]>(`${this.url}`)
    .pipe(map((results: Antenne[]) => results['records'].map(antenne => new Antenne(antenne)))
    );
  }

  getMoreAntennes(start: number): Observable<Antenne[]> {
    return this.http.get(`${this.url}` + "&start=" + start)
    .pipe(map((results: Antenne[]) => results['records'].map(antenne => new Antenne(antenne)))
    );
  }

  /**
  * Récupère les données de l'API opendatasoft
  * 
  * 
  * @param {string} depNumber	 Numéro de département
  * @returns Observable avec les résultats de la recherche
  */
  searchData(depNumber: number): Observable<Antenne[]> {
    return this.http.get(`${this.url}&refine.dep_code=${encodeURI(depNumber.toString())}`)
    .pipe(map((results: Antenne[]) => results['records'].map(antenne => new Antenne(antenne)))
    );
  }

  changeSearchMode(type: string) {
    this.searchType = type;
  }
 
  /**
  * Récupère les informations détaillées d'une antenne
  * 
  * @param {string} id le recordid de l'antenne
  * @returns Observable avec les informations
  */
   getDetails(id: string): Observable<Antenne[]> {
    return this.http.get<Antenne[]>(`${this.url}&refine.recordid=${id}`)
    .pipe(map((results: Antenne[]) => results['records'].map(antenne => new Antenne(antenne)))
    );
  }
}