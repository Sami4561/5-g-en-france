# Projet Web Transverse : Ionic/Angular

## Ambroise GABERT - Sami BÉHAR - LPRO Web

## But de l'application

Cette application répertorie l'ensemble des antennes 5G déployées en France. Les données sont récupérées grâce à une [API Opendatasoft](https://public.opendatasoft.com/explore/dataset/buildingref-france-arcep-mobile-site-5g/information/?disjunctive.op_name&disjunctive.frequency&disjunctive.com_code&disjunctive.com_name&disjunctive.epci_code&disjunctive.epci_name&disjunctive.dep_code&disjunctive.dep_name&disjunctive.reg_code&disjunctive.reg_name).  

Sur la page principale de l'application, toutes les antennes sans critère particulier sont affichées dont leur opérateur, leur(s) fréquence(s) et leur commune.  
On peut cliquer sur une antenne pour avoir une page de détail comprenant sa localisation exacte sur une carte Google Maps ainsi que sa date d'installation.  

Il est possible de trier les antennes par opérateur ou par fréquence, on peut également les rechercher par numéro de département grâce à la barre de recherche.  

Dans la page "Antennes proches", il faut d'abord sélectionner sa région. Ensuite, l'application affiche la distance des antennes par rapport à la localisation de l'utilisateur. On peut également spécifier un certain rayon pour par exemple n'afficher que les antennes à moins de 20 kilomètres de soi-même. Les mêmes filtres mentionnés précédemment sont aussi disponibles.

## Lancement du projet

<code>npm install @capacitor/google-maps</code>  
<code>npm install @capacitor/geolocation</code>  
<code>ionic serve</code>  

Si problème avec le module Google Maps, supprimer le dossier /node_modules/@types/googlemaps.